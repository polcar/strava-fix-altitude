#!/bin/bash

rm -rf *.egg-info
rm -rf .cache .coverage htmlcov .pytest_cache
find -type d -name '__pycache__' | xargs rm -fr
find -type f -name '*.pyc' | xargs rm -f
rm -f geckodriver.log

