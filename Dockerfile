FROM joyzoursky/python-chromedriver:3.7-alpine3.8
MAINTAINER Jiri Polcar "polcar@physics.mini.cz"

RUN apk add --update py3-psutil python3 python3-dev py3-pip  build-base linux-headers && rm -rf /var/cache/apk/*


ADD requirements.txt /
RUN pip3 install -r /requirements.txt

ADD . /app
RUN cd /app && python setup.py install
CMD strava-fix-altitude --debug --port $PORT --login $LOGIN --password $PASSWORD

