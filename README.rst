
Strava Fix Altitude
===================


Installation
------------

.. code-block:: sh

    pip install git+https://bitbucket.org/polcar/strava-fix-altitude.git


Docker build
------------

.. code-block:: sh

    docker build -t polcar/strava-fix-altitude -f Dockerfile .
    docker run --rm -it -p 9999:9999 -e PASSWORD='password' -e LOGIN='email' -e PORT=9999 polcar/strava-fix-altitude:live
    docker push polcar/strava-fix-altitude


Setup Service
-------------

.. code-block:: sh

    apt-get install tmux docker.io
    tmux att -t polcar-strava || tmux new -s polcar-strava
    docker pull polcar/strava-fix-altitude:live
    docker run --rm -it -p 9999:9999 -e PASSWORD='password' -e LOGIN='email' -e PORT=9999 polcar/strava-fix-altitude:live



Commandline usage
-----------------

.. code-block:: sh

    strava-fix-altitude --port 9999 --login 'email' --password 'password'

